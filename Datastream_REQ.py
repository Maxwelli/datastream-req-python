 #Create Schema for request 
        DTReq = DataSetRequest()
        DTReq.name = 'NE_Div 2 FRCST-OPS_Sales'
        DTReq.description = 'Div 2 Forecasting Sales #s'
        DTReq.schema = Schema([Column(ColumnType.STRING, 'X1'),
                             Column(ColumnType.STRING, 'X2'),
                             Column(ColumnType.STRING, 'X4')
                             Column(ColumnType.STRING, 'X5'),
                             Column(ColumnType.STRING, 'X6'),
                             Column(ColumnType.STRING, 'X7'),
                             Column(ColumnType.STRING, 'X8'),
                             Column(ColumnType.STRING, 'X9'),
                             Column(ColumnType.STRING, 'X10'),
                             Column(ColumnType.STRING, 'X11'),
                             Column(ColumnType.STRING, 'X12'),
                             Column(ColumnType.INT, 'Z1'),
                             Column(ColumnType.STRING, 'X13'),
                             Column(ColumnType.STRING, 'X14'),
                             Column(ColumnType.STRING, 'X315'),
                             Column(ColumnType.INT, 'Z2'),
                             Column(ColumnType.INT, 'Z3'),
                             Column(ColumnType.INT, 'Z4'),
                             Column(ColumnType.INT 'Z5'),
                             Column(ColumnType.INT, 'Z6'),
                             Column(ColumnType.STRING, 'X16'),
                             Column(ColumnType.STRING, 'X17'),
                             Column(ColumnType.STRING, 'X18'),
                             Column(ColumnType.INT 'Z7'),
                             Column(ColumnType.STRING, 'X17'),
                             Column(ColumnType.STRING, 'X18'),
                             Column(ColumnType.INT, 'Z8'),
                             Column(ColumnType.STRING, 'X19'),
                             Column(ColumnType.INT, 'Z9'),
                             Column(ColumnType.STRING, 'X20'),
                             Column(ColumnType.INT, 'Z10'),
                             ,
                             ])

#Create Request
        stream_request = CreateStreamRequest(DTReq, UpdateMethod.REPLACE


#Create stream dataset (Replace X and Z with Dataset Name and ID Respectively)
		stream = streams.create(stream_request)
        domo.logger.info("Created Stream {} containing the new DataSet {}"
                         .format(stream['id'], stream['x4']['Z5']))


